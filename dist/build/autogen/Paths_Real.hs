module Paths_Real (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [0,1,0,0], versionTags = []}
bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/home/vilya/.cabal/bin"
libdir     = "/home/vilya/.cabal/lib/Real-0.1.0.0/ghc-7.4.1"
datadir    = "/home/vilya/.cabal/share/Real-0.1.0.0"
libexecdir = "/home/vilya/.cabal/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catchIO (getEnv "Real_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Real_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "Real_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Real_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
