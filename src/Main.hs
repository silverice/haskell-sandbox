import Data.Char
import Graphics.Rendering.Chart.Simple
--import Hepers
import Shoes

divide :: Double -> Double -> Double
divide a1 = \a2 -> a1 / a2

choose :: String -> String
choose str
	| str == "127.0.0.1" = "local-tail"
	| otherwise = str

listOfColours :: [String] -> String -> [String]
listOfColours list prefix =
	[prefix ++ "-" ++ str | str <- list, str /= prefix]
	--map (\str -> prefix ++ "-" ++ str) list

data TransportLayer = TCP | UPD | SPX
	deriving (Show, Enum)

--descriptionOf :: TransportLayer -> String
--descriptionOf protocol =
--	case protocol of 
--		TCP -> "TCP"
--		UPD -> "UDP"
--		SPX -> "SPX"


data Vector3 = Vector3 {
	x :: Double,
	y :: Double,
	z :: Double
}

descriptionOfVector :: Vector3 -> String
descriptionOfVector vector =
	"x " ++ show (x vector) ++ " y " ++
		show (y vector) ++ " z " ++ show (z vector)


square :: Double -> Double
square a = a * a

squareSumm :: (Double -> Double) -> [Double] -> Double
squareSumm func arr =
	foldl (\ acc x -> acc + (square . func) x) 0.0 arr


binaryMap :: (a -> a -> a) -> [a] -> [a]
binaryMap _ [] = []
binaryMap _ [x] = []
binaryMap func (x:rest) = func x (head rest) : (binaryMap func rest)


smooth :: (Double -> Double) -> Double -> Double -> Double
smooth func x dx = (func x + func (x + dx)) / 2.0



main :: IO ()
main =
	Shoes.shoesTest
	--shoesTest
	--print [TCP ..]
	--print $ show range
	--print (binaryMap (\ x y -> x * y) range)
	--plotPNG "foo.png" range signal "-" meanSquare "-" smoothFunc "."
	where
		delta = 1.0
		range = [1.0,(1.0 + delta)..50::Double]
		signal x = sin x + 0.3 * cos (x * 9.0) + sin (x * 0.5) + 0.2 * cos(x  * 18) + 4 * cos(x * 0.1)
		meanSquare _ = sqrt $ (squareSumm signal range) / (fromIntegral $ length range)
		smoothFunc x = smooth (signal) x 1.0
	--plotWindow [0,0.1..5::Double] sin 
	--print $ descriptionOfVector vec
	--where vec = Vector3 { x = 0.5, y = 0.2, z = 1.0}
	--print $ (listOfColours ["yellow", "carmine", "orange", "white"] "white")

