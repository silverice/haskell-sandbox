module Shoes where

data SmellLevel = Usual | DeadRat | PostApocalyptic
	deriving (Enum, Show)

class Shoe shoe where
	smell :: shoe -> SmellLevel
	color :: shoe -> String

data Valenky = Valenky

instance Shoe Valenky where
	smell Valenky = PostApocalyptic
	color Valenky = "grey" 

instance Show Valenky where
	show Valenky = (show . smell $ Valenky) ++ " " ++ (show . color $ Valenky)

--instance (Shoe a) => Show (Shoe a) where
--	show a = (show . smell $ a) ++ " " ++ (show . color $ a)


shoesTest :: IO ()
shoesTest = print $ show Valenky
